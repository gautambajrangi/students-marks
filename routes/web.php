<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\MarkSheetController;
use App\Http\Controllers\DatatableController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(array('middleware' => ['auth']), function () {


Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('student/listing', [DatatableController::class, 'index'])->name('student.listing');
Route::get('student/getData', [DatatableController::class, 'getData'])->name('get.student.listing');
Route::get('marks/listing', [DatatableController::class, 'indexMarks'])->name('marks.listing');
Route::get('marks/getData', [DatatableController::class, 'getData1'])->name('get.marks.listing');
Route::post('student/getlist', [StudentController::class, 'getlist'])->name('student.getlist');
Route::post('student/getSubjectMarks', [StudentController::class, 'getSubjectMarks'])->name('student.get-subject-marks');

Route::resources([
    'student' => StudentController::class,
    'marks' => MarkSheetController::class,
]);


//bn$Sc46Fx2@fvc



});









require __DIR__.'/auth.php';
