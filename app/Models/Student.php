<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
    	// 'id', using as // roll_number
        'name',
        'image',
        'class_number',
    ];


    protected $appends = [
        'fullurl_image',
        'total_marks'
    ];

    public function getFullurlImageAttribute(){
        if($this->image!=''){
            return asset('storage/images/'.$this->image);
        }
    }

    public function getTotalMarksAttribute(){
        return MarkSheet::where('student_id', $this->id)->sum('marks');
    }


    public function marks(){
        return $this->hasMany(MarkSheet::class, 'student_id', 'id');
    }

    public function percentile(){
        return $this->hasOne(Percentile::class, 'student_id', 'id')->where('exam_id', 1);
    }



}
