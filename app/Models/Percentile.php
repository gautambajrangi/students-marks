<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Percentile extends Model
{
    use HasFactory;

    protected $fillable = [
        'exam_id',
        'student_id',
        'total_marks',
        'marks',
        'percentage',
    ];

    public function student(){
        return $this->hasOne(Student::class, 'id', 'student_id');
    }
}
