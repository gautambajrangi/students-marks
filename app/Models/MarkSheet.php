<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MarkSheet extends Model
{
    use HasFactory;


    protected $fillable = [
        'exam_id',
        'student_id',
        'subject_id',
        'marks',
    ];

    public function student(){
        return $this->hasOne(MarkSheet::class, 'student_id', 'id');
    }

    public function subject(){
        return $this->hasOne(Subject::class, 'subject_id', 'id');
    }


}
