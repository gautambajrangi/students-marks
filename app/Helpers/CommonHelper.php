<?php


function randomString($length_of_string = 8) {
    $chr = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
    $randomString = ''; 
  
    for ($i = 0; $i < $length_of_string; $i++) { 
        $index = rand(0, strlen($chr) - 1); 
        $randomString .= $chr[$index]; 
    }   
    return $randomString; 
}

function dde($arr){
    echo "<pre>"; print_r($arr); die;
}


function qry($str,$return_in_array=0){
    $data = DB::select($str);
    if($return_in_array!=1){
        return $data;
    }
    else
    {
        return json_decode(json_encode($data), true);
    }
}

function getConst($key=''){
    if(trim($key=='')){
        return NULL;
    }
    else
    {
        return Config::get('constants.'.$key);
    }
}

function sendPath($dir=''){
    return asset('storage').'/'.$dir.'/';
}

function RemoveSpecialChar($str) {
    $res = str_replace(array( '\'', '"', ',' , ';', '<', '>' ), '', $str);
    return $res;
}

function stringToSlug($string){
    $slug = preg_replace('/[^a-zA-Z0-9 -]/','', trim($string));
    $slug = str_replace(' ','-', $slug);
    return strtolower($slug);
}


function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');
    $realEnd = new DateTime($end);
    $realEnd->add($interval);
    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
    foreach($period as $date) {                 
        $array[] = $date->format($format); 
    }
    return $array;
}

function dateDiff($date1, $date2) {
    return round((strtotime($date2) - strtotime($date1)) / 86400);
}


