<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Examination;
use App\Models\MarkSheet;
use App\Models\Subject;
use App\Models\Student;
use App\Traits\SampleTrait;
use DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class DatatableController extends Controller
{ 
    use SampleTrait;

    public function index()
    {
        $listExams = Examination::orderBy('created_at', 'DESC')->limit(5)->get();
        $data = ['label' => 'Student Listing', 'listExams' => $listExams];
        return view('student.listing', $data);
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {

        $data = Student::select('id', 'name','image','class_number');

        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">Nothing</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    public function indexMarks()
    {
        $listSubject = Subject::orderBy('created_at', 'DESC')->limit(50)->get();
        $data = ['label' => 'Student Marks Listing', 'listSubject' => $listSubject];
        return view('marks.listing', $data);
    }

    public function getData1(Request $request){
        $columns = array(
            0 => 'st.id',
            1 => 'st.name',
            2 => 'st.class_number',
            3 => 'ms.marks',
            4 => 'ms.created_at',
        );



        $subject_id_sql = '';
        if($request['subject_id']!=''){
            $subject_id_sql = ' and ms.subject_id='.$request['subject_id'];
        }

        $sql = "select st.*,ss.subject_name,ms.marks as msmarks, ms.created_at as mscreated_at from mark_sheets ms 
        INNER JOIN students st ON st.id=ms.student_id 
        LEFT JOIN subjects ss ON ss.id=ms.subject_id 
        WHERE ms.exam_id='1'  ".$subject_id_sql;
        $query = qry($sql);
        $totalData = count($query);
        $totalFiltered = $totalData;

        if(!empty($request['search']['value'])) {   
            $searchString = "'%" . RemoveSpecialChar(trim($request['search']['value'])) . "%'"; 
            $sql .= " AND ( st.name LIKE " . $searchString;
            $sql .= " OR st.class_number LIKE " . $searchString;
            $sql .= " OR ms.marks LIKE " . $searchString . ")";
        }

        $query = qry($sql);
        $totalFiltered = count($query);

        $sql .= " ORDER BY " . $columns[$request->order[0]['column']] . "   " . $request->order[0]['dir'] . " LIMIT " . $request->start . " ," . $request->length . "   ";  
        $query = qry($sql);
        $data = array();
        $cnts = $request->start + 1;

        foreach ($query as $row) {

            $nestedData = array();

            $nestedData[] = $row->id;
            $nestedData[] = $row->name;
            $nestedData[] = $row->class_number;
            $nestedData[] = $row->subject_name." - ".$row->msmarks;
            $nestedData[] = '';
            $nestedData['DT_RowId'] = "r" . $row->id;
            $data[] = $nestedData;
            $cnts++;
        }

        $json_data = array(
            "draw" => intval($request['draw']), 
            "recordsTotal" => intval($totalData), 
            "recordsFiltered" => intval($totalFiltered), 
            "data" => $data
        );
        echo json_encode($json_data);
    }

}
