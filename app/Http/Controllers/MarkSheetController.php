<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\MarkSheet;
use App\Models\Examination;
use App\Models\Subject;
use App\Models\Percentile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Session;

class MarkSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listExams = Examination::orderBy('created_at', 'DESC')->limit(5)->get();
        $data = ['label' => 'MarkSheet', 'listExams' => $listExams];
        return view('marks.add', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $array = [];
        try {
        $validator = Validator::make($request->all(), [ 
            'student_id' => 'required|integer|exists:App\Models\Student,id',
            'exam_id' => 'required|integer|exists:App\Models\Examination,id',
            'subject_id' => 'required|array',
            'subject_id.*' => 'required|integer|exists:App\Models\Subject,id',
            'marks' => 'required|array',
            'marks.*' => 'required|numeric|between:0,100',
        ]);
        if($validator->fails()) {  
            Session::flash('msg', $validator->errors()->first());
            Session::flash('cls', 'danger');
            return redirect()->back(); 
        }

        $student = User::find($request->student_id);

        $i = 0;
        foreach ($request->subject_id as $key => $value) {
            $updates = [
                'student_id' => $request->student_id,
                'exam_id' => $request->exam_id,
                'subject_id' => $value,
                'marks' => $request->marks[$i],
            ];

            $checks = [
                'student_id' => $request->student_id,
                'exam_id' => $request->exam_id,
                'subject_id' => $value,
            ];

            MarkSheet::updateOrCreate($checks, $updates);

            $updates = [
                'student_id' => $request->student_id,
                'exam_id' => $request->exam_id,
                'total_marks' => count($request->subject_id) * 100,
                'marks' => array_sum($request->marks),
                'percentage' => array_sum($request->marks) / count($request->subject_id),
            ];
            $checks = [
                'student_id' => $request->student_id,
                'exam_id' => $request->exam_id,
            ];
            Percentile::updateOrCreate($checks, $updates);
            
            $i++;
        }

        Session::flash('msg', "successful");
        Session::flash('cls', 'success');
        return redirect()->back(); 

        } catch (\Exception $e) {
            Session::flash('msg', $e->getMessage());
            Session::flash('cls', 'danger');
            return redirect()->back(); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FarmHouse  $farmHouse
     * @return \Illuminate\Http\Response
     */
    public function show(MarkSheet $markSheet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FarmHouse  $farmHouse
     * @return \Illuminate\Http\Response
     */
    public function edit(MarkSheet $markSheet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FarmHouse  $farmHouse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarkSheet $markSheet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FarmHouse  $farmHouse
     * @return \Illuminate\Http\Response
     */
    public function destroy(MarkSheet $markSheet)
    {
        //
    }
}
