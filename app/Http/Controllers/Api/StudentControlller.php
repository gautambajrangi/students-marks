<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Examination;
use App\Models\Percentile;


class StudentControlller extends Controller
{
    

    public function index()
    {
    	$lastExam = Examination::orderBy('id','DESC')->first(['id']);
        // $listing = Student::paginate(100)->sortByDesc('total_marks');
        $listing = Percentile::with('student')
        ->where('exam_id', $lastExam->id)
        ->orderBy('percentage', 'DESC')
        ->paginate(100); 
        return response()->json($listing, 200);
    }
}
