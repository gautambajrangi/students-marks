<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\MarkSheet;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ['label' => 'Student Marks Listing'];
        return view('student.listing', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function getSubjectMarks(Request $request)
    {
        try {
        $validator = Validator::make($request->all(), [ 
            'student_id' => 'required|integer|exists:App\Models\Student,id',
            'exam_id' => 'required|integer|exists:App\Models\Examination,id',
        ]);
        if($validator->fails()) {  
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),
                'data'    => getConst('emptyData'),
            ]);   
        }

        $marks = MarkSheet::where('student_id', $request->student_id)->where('exam_id', $request->exam_id)->get();
        $subjects = Subject::get();

        return response()->json([
            'success' => true,
            'message' => "",
            'data'    => [
                'marks' => $marks,
                'subjects' => $subjects,
            ],
        ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data'    => getConst('emptyData'),
            ]); 
        }
    }


    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FarmHouse  $farmHouse
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FarmHouse  $farmHouse
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FarmHouse  $farmHouse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FarmHouse  $farmHouse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }

    public function getlist(Request $request){
        $response = [];
        try {
            $searchTerm = '';
            if(isset($request->searchTerm)) {
                $searchTerm = RemoveSpecialChar($request->searchTerm);
            }
            $data = Student::where(function($query) use ($searchTerm) {
                    if($searchTerm!='') {
                        $query->where('name','LIKE', $searchTerm.'%');
                        $query->orWhere('id','LIKE', $searchTerm.'%');
                    }
            })
          ->orderBy('id','DESC')
          ->limit(10)
          ->get(['id', 'name', 'image']);

          foreach ($data as $key => $value) {
            $response[] = [
                'text' => $value->name." - ".$value->id,
                'id' => $value->id,
                // 'image' => $value->image,
                'fullurl_image' => $value->fullurl_image,
            ];
          }
            return response()->json($response);
        } catch (\Exception $e) {
            dde($e->getMessage());
            return $response;
        }
    }
}
