<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'class_number' => mt_rand(1, 12),
            'image' => $this->faker->image('public/storage/images', 200, 200, null, false),
        ];
    }
}
