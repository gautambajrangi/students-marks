<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Examination;

class ExaminationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Examination::truncate();
        Examination::create(["name" => "Annual Exam-2022"]);
    }
}
