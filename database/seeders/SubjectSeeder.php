<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subject;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::truncate();
        Subject::insert([
        	["subject_name" => "Hindi"],
        	["subject_name" => "English"],
        	["subject_name" => "Mathmatics"],
        	["subject_name" => "Science"],
        	["subject_name" => "Drawing"],
        ]);
    }
}
