@extends('layout.master')
@section('title', $label." | ".env('APP_NAME'))
@section('meta_description', @$Metatag->description)
@section('meta_keywords', @$Metatag->meta_keywords)
@stop 
@section('css_cdn')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/dataTables.bootstrap5.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert2/sweetalert2.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@stop
@section('content')
<main id="main" class="main"> 
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">{{ $label }}</h5>


        <div class="col-md-12"> @if(Session::get("msg")!='')
      <div class="alert alert-{{ Session::get('cls') }} alert-dismissible fade show" role="alert">{{ Session::get('msg') }}</div>
      @endif </div>
          
          <!-- Multi Columns Form -->
          <form class="row g-3" method="post" enctype="multipart/form-data" action="{{ route('marks.store') }}">
            @csrf     
            <div class="row">       
            <div class="col-md-5">
                <label class="form-label">Select Exam*</label>
                  <select class="form-select" name="exam_id" id="exam_id" required>
                      @foreach($listExams as $row)
                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                      @endforeach
                  </select>
                 </div>
                <div class="col-md-7">
              <label class="form-label">Select Student*</label>
              <select class="form-select Select2ByIdClass" aria-label="Default select example" id="student_id" name="student_id" required>
                <option selected="" value="">Search and Select Any One</option>
              </select>
            </div>
          </div>
          
          <hr />
          
          <div class="row">
          <div class="col-md-8"> 
          <label class="form-label">Subject</label> 
          </div>
          <div class="col-md-4">  
          <label class="form-label">Marks</label> 
          </div>
          </div>
          
          <div id="marks-div">
          
          </div>

          </div>
            
    <hr />
            <div class="text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
          <!-- End Multi Columns Form --> 
          
        </div>
      </div>
    </div>
  </section>
</main>
@stop
@section('js_cdn') 
<script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script> 
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap5.min.js') }}"></script> 
<script src="{{ asset('assets/vendor/sweetalert2/sweetalert2.min.js') }}"></script> 
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
@stop 

@section('script') 
<script type="text/javascript">
$('body').on('change', '#student_id', function () {
	var ehtml = '';
	var student_id = $(this).val();
	var exam_id = $('#exam_id').val();
	if (student_id=='' || exam_id=='') {
		alert('field required.');
        return false;
    }
    $.ajax({
         url: "{{ route('student.get-subject-marks') }}",
         data: {
              _token: '{{ csrf_token() }}',
			  student_id: student_id,
			  exam_id: exam_id,
         },
         type: 'post',
         dataType: 'json',
         success: function(e) {
			 if(e.success==false){
				 showSweetAlert('Oops', e.message, 0);
			 }
			 else {

        for (var i = 0; i < e.data.subjects.length; i++)
        {
          var mark = '';
          for (var j = 0; j < e.data.marks.length; j++) {
            if(e.data.subjects[i].id==e.data.marks[j].subject_id){
               mark = e.data.marks[j].marks;
              break;
            }
          }
        
				  ehtml +='<div class="row"><div class="col-md-6"> <input type="text" class="form-control" value="'+e.data.subjects[i].subject_name+'" readonly="readonly" /><input type="hidden" name="subject_id[]" value="'+e.data.subjects[i].id+'"></div><div class="col-md-6"><input type="number" class="form-control allownumber" value="'+mark+'" name="marks[]" required="required" /></div><hr>';
        }

        $('#marks-div').html(ehtml);
				 
			 }
             },
             error: function() {
                showSweetAlert('Something Went Wrong!', 'please refresh page and try again.', 0);
          }
    });		
});
</script> 
@include('student.get-in-dropdown') 
@stop 