<aside id="sidebar" class="sidebar">
  <ul class="sidebar-nav" id="sidebar-nav">
    <li class="nav-item {{ ( request()->is('/dashboard/*') ) ?  'active' : '' }}" data-bs-toggle="{{ ( request()->is('/dashboard/*') ) ?  'collapse' : '' }}"> <a class="nav-link " href="{{ route('dashboard') }}"> <i class="bi bi-grid"></i> <span>Dashboard</span> </a> </li>
    <!-- End Dashboard Nav -->    
    <li class="nav-item {{ ( request()->is('/student/*') ) ?  'active' : '' }}" data-bs-toggle="{{ ( request()->is('/student/*') ) ? 'collapse' : ''  }}"> <a class="nav-link" href="{{ route('student.listing') }}"> <i class="bi bi-menu-button-wide"></i><span>Student</span> </a> </li>
    
    <li class="nav-item {{ ( request()->is('/marks/*') ) ?  'active' : '' }}" data-bs-toggle="{{ ( request()->is('/marks/*') ) ? 'collapse' : ''  }}"> <a class="nav-link" href="{{ route('marks.listing') }}"> <i class="bi bi-menu-button-wide"></i><span>Marks</span> </a> </li>
  </ul>
</aside>
