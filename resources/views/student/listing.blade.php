@extends('layout.master')
@section('title', $label." | ".env('APP_NAME'))
@section('meta_description', @$Metatag->description)
@section('meta_keywords', @$Metatag->meta_keywords)
@stop 
@section('css_cdn')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/dataTables.bootstrap5.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert2/sweetalert2.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@stop
@section('content')
<main id="main" class="main"> 
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">{{ $label }}
              <a href="{{ route('marks.index') }}">
              <button type="button" class="btn btn-primary float-end" id="addnewpostid"><i class="bi bi-plus me-1"></i> Add Marks</button>
              </a>
            </h5>
             @if(Session::get("msg")!='')
                  <p>  {{ Session::get('msg') }}</p>
              @endif 
            <!-- Table with stripped rows -->
            <table class="table datatable" id="table1">
              <thead>
                <tr>
                <th scope="col">Role No</th>
                  <th scope="col">Student Name</th>
                  <th scope="col">Class</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            <!-- End Table with stripped rows --> 
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
@stop
@section('js_cdn') 
<script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script> 
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap5.min.js') }}"></script> 
<script src="{{ asset('assets/vendor/sweetalert2/sweetalert2.min.js') }}"></script> 
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@stop 
@section('script') 
<script type="text/javascript">
$(document).ready(function () {
	listingStudents();
});

	
function listingStudents(){
    var table; 
    $.fn.dataTable.ext.errMode = 'none';
    $('#table1').on('error.dt', function(e, settings, techNote, message) {
        alert(message);
    });

    $(function () {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        searchDelay: 1000, 
        lengthMenu: [[10, 50, 100, 200, 500], [10, 50, 100, 200, 500]],
        pageLength: 10, 
        ajax: "{{ route('get.student.listing') }}",
        columns: [
		    {data: 'id', name: 'id'},
		    {data: 'name', name: 'name'},
			{data: 'class_number', name: 'class_number'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    });
}

</script> 
@stop 