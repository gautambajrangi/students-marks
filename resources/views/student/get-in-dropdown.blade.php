<script type="text/javascript">
if(typeof(includeid) == "undefined" || includeid == null) {
   var includeid = 0;
}
var select2ById = $(".Select2ByIdClass").select2({
  placeholder: "Search and Select Any One",
  width:"99%",
  closeOnSelect: true,
  templateResult: formatState,
    ajax: {
      url: "{{ route('student.getlist') }}",
      type: "post",
      dataType: 'json',
      delay: 250,
       data: function (params) {
        return {
          searchTerm: params.term ,
          _token:'{{ csrf_token() }}',
          'includeid': includeid,
        };
      },
      processResults: function (response) {
        return {
          results: response
        };
      },
      cache: true
    }
    
});

function formatState(oneData) {
  if(!oneData.id) {
    return oneData.text;
  }
  var imageHtml = $('<span><img src="'+oneData.fullurl_image+'" class="img-flag" style="max-height:30px; max-width:30px;" /> ' + oneData.text + '</span>');
  return imageHtml;
};
</script>
