<?php

return [
    'emptyData' => new \stdClass(),
    'is_active'    => [
        'true'    => 1,
        'false' => 0,
    ],

'BookingType'     => [ 
    'Fullday' => [ "shortkey" => "Fullday", "name" => "Full day"],
    'Halfday' => [ "shortkey" => "Halfday", "name" => "Half day"],
    'Custom' => [ "shortkey" => "Custom", "name" => "Custom"],
],

'BookingDurationBreakPoints' => [ 15, 30, 45, 60], // in minutes




];





